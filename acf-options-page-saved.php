<?php
/**
 * Plugin Name: ACF Options Page Saved
 * Description: A simple plugin that show you if the options page has been saved at least one time.
 * Version: 1.0.0
 * Author: Kofinorr
 * Author URI: https://kofinorr.damien-roche.fr/
 * Text Domain: krr-aops
 * Domain Path: /languages
 * License: GPLv3 or later
 */

namespace KrrAcfOptionsPageSaved;

/**
 * Class AcfOptionsPageSaved
 * @package KrrAcfOptionsPageSaved
 */
class AcfOptionsPageSaved
{
	const OPTION_NAME = 'krr_acf_options_pages_saved';

	/**
	 * AcfOptionsPageSaved constructor.
	 */
	public function __construct()
	{
		add_action('plugins_loaded', [$this, 'load_i18n']);
		add_action('admin_init', [$this, 'createStorageOption'], 10);
		add_action('acf/save_post', [$this, 'checkup'], 10);
		add_action('all_admin_notices', [$this, 'noticeRenderer']);
	}

	/**
	 * Load plugin textdomain.
	 */
	public function load_i18n() {
		load_plugin_textdomain(
			'krr-aops',
			false,
			dirname(plugin_basename(__FILE__)) . '/languages'
		);
	}

	/**
	 * Create the storage of the saved options pages
	 */
	public function createStorageOption()
	{
		add_option(self::OPTION_NAME, []);
	}

	/**
	 * Retrieve the current options page ID
	 * @return bool|string
	 */
	private function getOptionsPageId()
	{
		/* Get current page */
		$currentScreen = get_current_screen();

		/* Get current page ID */
		$pageID = isset($_GET['page']) ? $_GET['page'] : 'unknownPage';

		/* Get all Acf options pages */
		$rule          = [
			'param'    => 'options_page',
			'operator' => '==',
			'value'    => 'acf-options',
			'id'       => 'rule_0',
			'group'    => 'group_0'
		];
		$rule         = acf_get_valid_location_rule($rule);
		$optionsPages = acf_get_location_rule_values($rule);

		/* Return the page ID if we are on an options page */
		if (!strpos($currentScreen->id, 'acf-options')
			&& (empty($optionsPages) || !array_key_exists($pageID, $optionsPages))
		) {
			return false;
		} else {
			return $pageID;
		}
	}

	/**
	 * Check if the current options page has already save at least one time
	 */
	public function checkup()
	{
		/* Retrieve current options page ID */
		$optionsPageId = $this->getOptionsPageId();

		if ($optionsPageId === false) {
			return false;
		}

		/* Get the list of pages saved */
		$optionsPagesChecked = get_option(self::OPTION_NAME);

		// Si la page n'a pas encore mise à jour, on ajoute son ID au tableau
		if (!in_array($optionsPageId, $optionsPagesChecked)) {
			array_push($optionsPagesChecked, $optionsPageId);
			update_option(self::OPTION_NAME, $optionsPagesChecked);
		}
	}

	/**
	 * Add a notice if the page has not been already saved
	 */
	public function noticeRenderer()
	{
		/* Retrieve current options page ID */
		$optionsPageId = $this->getOptionsPageId();

		/* Add message only if the options page has already been saved */
		if ($optionsPageId !== false && !in_array($optionsPageId, get_option(self::OPTION_NAME))) {
			echo '<div class="notice notice-error">
				<p>' . __("This options page has not been saved !", "krr-aops") . '</p>
			</div>';
		}
	}
}

new AcfOptionsPageSaved();